
#cardinal directions
directions = c("N","NE","E","SE","S","SW","W","NW")
quantdirections = c(pi,pi+(1/4)*pi,pi+(pi/2),pi+(3/4)*pi,0,pi/4,pi/2,(3/4)*pi)



getAngleBetween = function(dir1,dir2){
	i1 = which(directions==dir1)
	i2 = which(directions==dir2)
	ang = abs(i1-i2)*pi/4
	if(ang>pi) ang = 2*pi-ang
	ang
}

vectors = matrix(c(0,1,1,1,1,0,1,-1,0,-1,-1,-1,-1,0,-1,1),ncol=2,byrow=TRUE)

getIndex = function(dir){
	which(directions==dir)
}

#radiant angles
angles = c(0,(1/4)*pi,(1/2)*pi,(3/4)*pi,pi)

#orientation table
orientation = matrix(c(angles[1:5],angles[4:2],
			     angles[2],  angles[1:5], angles[4:5],
			     angles[3:2],angles[1:5], angles[4],
			     angles[4:2],angles[1:5],
			     angles[5:2],angles[1:4],
			     angles[4],  angles[5:1], angles[2:3],
			     angles[3:4],angles[5:1], angles[2],
			     angles[2:4],angles[5:1]),ncol=8)

getDirection = function(angle,direction){
  angles = c(0,(1/4)*pi,(1/2)*pi,(3/4)*pi,pi)
	if(is.nan(angle)) NaN else{
    if(angle>pi) angle = 2*pi-angle
	  index = (which(abs(angles-angle)==min(abs(angles-angle)))-1)%%6
	  offset = which(directions==direction)
	  a = (offset+index)%%8
	  if (a==0) {a=8}
	  b = (offset-index)%%8
	  if (b==0) {b=8}
	  unique(c(directions[a],directions[b]))
	}
}

getAlpha = function(dir1,dir2){
	orientation[which(directions==dir1),which(directions==dir2)]
}

getLocations = function(tA,tB,coordsA,coordsB,distA,distB,speed,direction){

}

toside = matrix(c(NaN,"left","left","left",NaN,"right","right","right",
                    "right",NaN,"left","left","left",NaN,"right","right",
                    "right","right",NaN,"left","left","left",NaN,"right",
                    "right","right","right",NaN,"left","left","left",NaN,
                    NaN,"right","right","right",NaN,"left","left","left",
                    "left",NaN,"right","right","right",NaN,"left","left",
                    "left","left",NaN,"right","right","right",NaN,"left",
                    "left","left","left",NaN,"right","right","right",NaN),ncol=8)
                    
plumeto = function(sensordir,plumedir){
  toside[which(directions==sensordir),which(directions==plumedir)] 
}

flip = function(side){
  result = NaN
  if (side=="left") result = "right"
	if (side=="right") result = "left"
	result
}