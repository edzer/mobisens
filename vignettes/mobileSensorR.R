### R code from vignette source '/home/edzer/git/mobisens/vignettes/mobileSensorR.Rnw'
### Encoding: UTF-8

###################################################
### code chunk number 1: mobileSensorR.Rnw:78-96
###################################################
library(mobisens)
pth = system.file("data",package="mobisens")
fnb="mp"			# string the file names begin with, followed by time step
fne=".csv"        # string the file names end with (after time step)
x = 8000          # x coordinate of the starting point
y = 3000          # y coordinate of the starting point
t = 20            # time step to start the simulation with
xmin = 500        # x coordinate the grid begins with
ymin = 500        # y coordinate the grid begins with
deltax = 100      # distance represented by one pixel in x direction
deltay = 100      # distance represented by one pixel in y direction
nx = 246          # number of pixels in x direction
ny = 246          # number of pixels in y direction
thres = 0.0001    # threshold measurement value, minimum measurable field value
# start sensor at (0,0):
sensorStart(pth, fnb, fne, xmin, ymin, deltax, deltay, nx, ny, thres)
# or start sensor at specific location:
sensorStartAt(x, y, t, pth, fnb, fne, xmin, ymin, deltax, deltay, nx, ny, thres)


###################################################
### code chunk number 2: mobileSensorR.Rnw:102-107
###################################################
turn(45)           # turn clockwise 45 degrees
forward(4000,400)  # move forward 4000 distance units, 400 distance units per time step
turn(-80)          # turn anti-clockwise 80 degrees   
forward(3000,500)  # move forward 3000 distance units, 500 distance units per time step
getmobisensTrajectory()


###################################################
### code chunk number 3: mobileSensorR.Rnw:115-125
###################################################
e1 = new.env()
e2 = new.env()
sensorStart(pth, fnb, fne, xmin, ymin, deltax, deltay, nx, ny, thres, mobisensEnv=e1)
turn(45,e1)
forward(5000,1000,e1)
sensorStart(pth, fnb, fne, xmin, ymin, deltax, deltay, nx, ny, thres, mobisensEnv=e2)
turn(20,e2)
forward(4000,800,e2)
getmobisensTrajectory(e1)
getmobisensTrajectory(e2)


###################################################
### code chunk number 4: mobileSensorR.Rnw:134-145
###################################################
coords=data.frame(x=c(8000,11000,13000),y=c(7000,5000,9000))
ts=c(20,30,40)        # time steps when sensors measure the field value
ps = placeStaticSensors(coords,ts)
ps
toffset = 0
deltat = 4
nt = 36
i = c(0:(nt-1))
ts = toffset+i*4      # time steps when sensors measure the field value
# 25 sensors in a random stratified sampling design
sr = stratified25sensors(ts)


###################################################
### code chunk number 5: mobileSensorR.Rnw:154-163
###################################################
# Plot of the grids at four time steps 20, 30, 40, and 50
plotGrids(pth,fnb,fne,thres,20,50,10,500,500,100,100,246,246)
# Plot of the grid at time step 150
plotGridT(pth,fnb,fne,thres,150,500,500,100,100,246,246)
#plotmobisens()	    # trajectory in color
#plotmobisensBw()    # trajectory with black/white symbols
par(mfrow = c(1, 2))
plotmobisens(ps)    # trajectory and static sensors in color, Figure 1(a)
plotmobisensBw(sr)  # trajectory and static sensors with black/white symbols, Figure 1(b)


###################################################
### code chunk number 6: mobileSensorR.Rnw:424-427
###################################################
data(scenarioResults)
aggregate(bacteriaResult$minDistToSource,by=list(bacteriaResult$m),FUN=mean)
aggregate(bacteriaResult$minDistToSource,by=list(bacteriaResult$m),FUN=sd)


###################################################
### code chunk number 7: mobileSensorR.Rnw:434-435
###################################################
spiralResult[order(spiralResult$minDistToSource)[1:15],]


###################################################
### code chunk number 8: mobileSensorR.Rnw:441-447
###################################################
spiralGood = spiralResult[which((spiralResult$deltaT<5)),]
spiralBad = spiralResult[which((spiralResult$deltaT>=5)),]
t.test(spiralGood$minDistToSource,spiralBad$minDistToSource,alt="less")
spiralGood = spiralResult[which((spiralResult$deltaTp<3)),]
spiralBad = spiralResult[which((spiralResult$deltaTp>=3)),]
t.test(spiralGood$minDistToSource,spiralBad$minDistToSource,alt="less")


###################################################
### code chunk number 9: mobileSensorR.Rnw:453-456
###################################################
spiralGood = spiralResult[which((spiralResult$deltaTp<3)&(spiralResult$deltaT<5)),]
aggregate(spiralGood$minDistToSource,by=list(spiralGood$m),FUN=mean)
aggregate(spiralGood$minDistToSource,by=list(spiralGood$m),FUN=sd)


###################################################
### code chunk number 10: mobileSensorR.Rnw:463-465
###################################################
planMean = aggregate(planarianResult$minDistToSource,by=list(planarianResult$m,planarianResult$d),FUN=mean)
planMean[order(planMean$x)[1:10],]


###################################################
### code chunk number 11: mobileSensorR.Rnw:471-474
###################################################
planGood = planarianResult[which((planarianResult$d>40)),]
planBad = planarianResult[which((planarianResult$d<=40)),]
t.test(planGood$minDistToSource,planBad$minDistToSource,alt="less")


###################################################
### code chunk number 12: mobileSensorR.Rnw:515-523
###################################################
# Good boundary tracking results with plume surrounding angle > 120
# and number of boundary crossing points > 6
btGood = btResult[which((btResult$ta>120)&(btResult$nCP>6)),]
pixeltotal = 250*250
correctPixels = ((pixeltotal-btGood$nP)-btGood$nFP)+btGood$nC
percentC = 100/pixeltotal*correctPixels
btGood = cbind(btGood,percentC)
btGood[order(btGood$percentC,decreasing=TRUE)[1:8],c(1:4,10:12)]


###################################################
### code chunk number 13: mobileSensorR.Rnw:590-598
###################################################
total = aggregate(ptResult$reasonings,by=list(ptResult$parameters),FUN=sum)
nfailed = function(x) {
  length(which(x!=201))
}
failed = aggregate(ptResult$t,by=list(ptResult$parameters),FUN=nfailed)
succ = total-failed
# % of successful plume tracking iterations for parameter combinations 1 - 6
100/total$x*succ$x


###################################################
### code chunk number 14: mobileSensorR.Rnw:711-713 (eval = FALSE)
###################################################
## library(devtools)
## install_bitbucket("mobisens", "edzer")


