# Scenario 3: Plume tracking
#------------
library(mobisens)
#plume model
py=0.659
qy=0.807
speed=200
dirs = c(rep(pi/4,200))
Q=5e5*20 
buildModel(py,qy,speed,dirs,Q)

thres=0.0001
pth = system.file("data",package="mobisens")
fnb = "mp"
fne = ".csv"  

ptResult = data.frame()
plot=FALSE       # set TRUE to plot every time step of the simulation
plotdir=""
finalplot=FALSE  # set TRUE to plot the final time step of the simulation

#run multiple times for each input (random component in the tracking algorithm)
for(p in c(1:6)){

for(i in c(1:10)){

sensorStartAt(7000,0,0,pth,fnb,fne,500,500,100,100,246,246,thres)
turn(10)
forward(6000,400)
nr=0

if(plot){
 setwd(plotdir)
 png(file="pt%03d.png")
}

# uncomment plumeTracking() with different parameters
# to test tracking performance with different input
while((length(getmobisensTrajectory()[,1])<200)&(getCurrentValue()>thres)){
 if(p==1) {try(silent=TRUE,plumeTracking(thres,cardDir="SW",plumespMin=100,plumespMax=150,plot=plot))}
 if(p==2) {try(silent=TRUE,plumeTracking(thres,plumespMin=100,plumespMax=150,plumeModel=TRUE,plot=plot))}
 if(p==3) {try(silent=TRUE,plumeTracking(thres,cardDir="SW",plumeModel=TRUE,plot=plot))}
 if(p==4) {try(silent=TRUE,plumeTracking(thres,cardDir="SW",plot=plot))}
 if(p==5) {try(silent=TRUE,plumeTracking(thres,plumeModel=TRUE,plot=plot))}
 if(p==6) {try(silent=TRUE,plumeTracking(thres,plumespMin=100,plumespMax=150,plot=plot))}
 nr=nr+1
}
if(plot) dev.off()
res = data.frame(t=length(getmobisensTrajectory()[,1]),reasonings=nr,parameters=p)

if(finalplot){
 png(file=paste("pt_p",p,"n",i,".png",sep=""))
 plotmobisens()
 dev.off()
}

ptResult = rbind(ptResult,res)

}
}

# ptResult
ptrMean = aggregate(ptResult$reasonings,by=list(ptResult$parameters),FUN=mean)
ptrMean[order(ptrMean$x),]
# length(which((ptResult$t==201)&(ptResult$parameters==1)))

total = aggregate(ptResult$reasonings,by=list(ptResult$parameters),FUN=sum)
nfailed = function(x){
 length(which(x!=201))
}
failed = aggregate(ptResult$t,by=list(ptResult$parameters),FUN=nfailed)
succ = total-failed
100/total$x*succ$x

