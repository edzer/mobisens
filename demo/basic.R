
library(mobisens)
pth = system.file("data",package="mobisens")
fnb="mp"	    # string the filenames begin with, followed by timestep
fne=".csv"      # string the filenames end with (after timestep)
x = 8000        # x coordinate of the starting point
y = 3000        #	y coordinate of the starting point
t = 20          # time step to start the simulation with
xmin = 500      #	x coordinate the grid begins with
ymin = 500      # y coordinate the grid begins with
deltax = 100    #	distance represented by one pixel in x direction
deltay = 100    #	distance represented by one pixel in y direction
nx = 246        #	number of pixels in x direction
ny = 246        #	number of pixels in y direction
thres = 0.0001  # threshold measurement value, minimum measurable field value

# start sensor at (0,0):
sensorStart(pth, fnb, fne, xmin, ymin, deltax, deltay, nx, ny, thres)

# or start sensor at specific location:
sensorStartAt(x, y, t, pth, fnb, fne, xmin, ymin, deltax, deltay, nx, ny, thres)

turn(45)           # turn clockwise 45 degrees
forward(4000,400)  # move forward 4000 distance units, 400 distance units per time step
turn(-80)          # turn anti-clockwise 80 degrees   
forward(3000,500)  # move forward 3000 distance units, 500 distance units per time step
getmobisensTrajectory()

plotmobisens()   # trajectory in color
plotmobisensBw() # trajectory with black/white symbols
plotGrids(pth,fnb,fne,thres,20,50,10,500,500,100,100,246,246)
plotGridT(pth,fnb,fne,thres,150,500,500,100,100,246,246)

# coordinates of three static sensors
coords=data.frame(x=c(8000,11000,13000),y=c(7000,5000,9000))
ts=c(20,30,40)
ps = placeStaticSensors(coords,ts)
plotmobisens(ps)
ps

# place 25 static sensors in a stratified random sampling
toffset = 0
deltat = 4
nt = 36
i = c(0:(nt-1))
ts = toffset+i*deltat
sr = stratified25sensors(ts)
plotmobisensBw(sr)

sensorStart(pth,fnb,fne,500,500,100,100,246,246,thres)
turn(45)
forward(1000,1000)
# Move in a space filling curve
wx = 5000    # width of the space to be filled in the direction perpendicular to the 
             # initial sensor orientation
wy = 6000    # width of the space to be filled in the direction of the initial sensor 
             # orientation
wz = 2000    # space between parallel parts of the sensor trajectory
speed = 1000 # sensor speed
dir = 0      # initial sensor direction is north
sfc(thres,wx,wy,wz,speed,dir)
plotmobisensBw()
