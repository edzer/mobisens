\name{mobisens-package}
\alias{mobisens-package}
\alias{mobisens}
\docType{package}
\title{Evaluating mobile sensing strategies}
\description{
This package can be used to simulate mobile sensors moving and measuring values in a two-dimensional field. It contains functions to perform several sensor movement strategies, for tracking a moving plume, for estimating the boundary of a plume, and for localizing the source of an emission.
}
\details{
\tabular{ll}{
Package: \tab mobisens\cr
Type: \tab Package\cr
Version: \tab 1.0\cr
Date: \tab 2013-11-27\cr
License: \tab GPL-2 cr
}
}
\author{Juliane Brink
Maintainer: Juliane Brink <juliane.brink@wwu.de>
}
\keyword{ package }
\examples{
# start the sensor and move through the field
pth = system.file("data",package="mobisens")
fnb="mp"
fne=".csv"
sensorStart(pth,fnb,fne,500,500,100,100,246,246,0)
turn(45)
forward(4000,400)
# plot sensor trajectory
plotmobisensBw()
# take a look at the coordinates and measured values
getmobisensTrajectory()
}
