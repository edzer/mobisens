\name{calcQuanAlpha}
\alias{calcQuanAlpha}
\title{Convert qualitative alpha to min/max quantitative alpha}
\description{
Convert qualitative alpha to min/max quantitative alpha.
}
\usage{
calcQuanAlpha(alpha)
}
\arguments{
  \item{alpha}{qualitative angle alpha, list of possible qualitative values}
}
\value{
min and max quantitative alpha
}
\references{
Brink, J. and Pebesma, E. (2013), Plume Tracking with a Mobile Sensor Based on Incomplete and Imprecise Information. Transactions in GIS. doi: 10.1111/tgis.12063
}
\author{Juliane Brink}
\examples{
calcQuanAlpha(c("sa","r","so"))
}
