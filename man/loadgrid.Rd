\name{loadgrid}
\alias{loadgrid}
\alias{loadgrid_bin}
\alias{loadgrid_no0}
\title{Load data grid from file.}
\description{Functions to load data grid from a file, as it is (loadgrid), as a binary field with 0 and 1 only (loadgrid_bin), log-transformed without 0 values (loadgrid_no0, is used by the plot functions).}
\usage{
loadgrid(path, fnb, fne, threshold, t, xmin, ymin, deltax, deltay, nx, ny)
loadgrid_bin(path, fnb, fne, threshold, t, xmin, ymin, deltax, deltay, nx, ny)
loadgrid_no0(path, fnb, fne, threshold, t, xmin, ymin, deltax, deltay, nx, ny)
}
\arguments{
  \item{path}{directory containing the grid data}
  \item{fnb}{string the filenames begin with, followed by timestep}
  \item{fne}{string the filenames end with (after timestep)}
  \item{threshold}{threshold value to decide about 0 and 1 in loadgrid_bin()}
  \item{t}{time step to be loaded}
  \item{xmin}{x coordinate the grid begins with}
  \item{ymin}{y coordinate the grid begins with}
  \item{deltax}{distance represented by one pixel in x direction}
  \item{deltay}{distance represented by one pixel in y direction}
  \item{nx}{number of pixels in x direction}
  \item{ny}{number of pixels in y direction}
}
\value{data grid of type SpatialGridDataFrame}
\author{Juliane Brink, Ragnar Warrlich}
\examples{
pth = system.file("data",package="mobisens")
fnb="mp"
fne=".csv"
gr = loadgrid(pth,fnb,fne,0.0001,10,500,500,100,100,246,246)
spplot(gr)
gr_bin = loadgrid_bin(pth,fnb,fne,0.0001,10,500,500,100,100,246,246)
spplot(gr_bin)
gr_no0 = loadgrid_no0(pth,fnb,fne,0.0001,10,500,500,100,100,246,246)
spplot(gr_no0)
}
