\name{bacteria}
\alias{bacteria}
\alias{planarian}
\alias{spiral_ferri}
\alias{PI}
\title{Find the source of an emission using chemotaxis algorithms}
\description{
Functions to locate the source of an emission using bio-inspired chemotaxis algorithms.
}
\usage{
bacteria(m,plot,plotdir,stop,sourceX,sourceY,mobisensEnv)
planarian(d,m,plot,plotdir,stop,sourceX,sourceY,mobisensEnv)
spiral_ferri(d_start,d_offset,K_mu,K_p,deltaT,deltaTp,plot,plotdir,stop,sourceX,sourceY,mobisensEnv)
PI(K_mu,K_p,deltaT,deltaTp,mobisensEnv)
}
\arguments{
  \item{m}{sensor speed}
  \item{d}{turning angle}
  \item{d_start}{sensor speed, and length of first spiral segment}
  \item{d_offset}{length added to spiral segment after each turn}
  \item{K_mu}{factor multiplied with mu (see equations in Ferry (2009)) }
  \item{K_p}{factor multiplied with P (see equations in Ferry (2009)) }
  \item{deltaT}{acquisition temporal window (see equations in Ferry (2009)) }
  \item{deltaTp}{temporal interval (see equations in Ferry (2009)) }
  \item{plot}{logical, set TRUE to plot every time step of the algorithm, optional, default is FALSE}
  \item{plotdir}{directory to save the plots, optional}
  \item{stop}{time step when the simulation stops, optional, default is 180}
  \item{sourceX}{x coordinate of the source (only for evaluation), optional, default is 5000 as in mobisens plume data set}
  \item{sourceY}{y coordinate of the source (only for evaluation), optional, default is 5000 as in mobisens plume data set}
  \item{mobisensEnv}{optional, use different environments for multiple mobile sensors}
}
\value{
data.frame with sensor trajectory parameters (number of time steps, number of turns, distance, maximum sensor speed)
}
\references{
G. Ferri, E. Caselli, V. Mattoli, A. Mondini, B. Mazzolai, and P. Dario. Spiral: A novel biologically-inspired
algorithm for gas/odor source localization in an indoor environment with no strong airflow. Robotics and Autonomous Systems, 57(4):393-402, 2009.
L. Marques, U. Nunes, and A.T. de Almeida. Olfaction-based mobile robot navigation. Thin Solid Films, 418(1):51-58, 2002.
R.A. Russell. Robotic location of underground chemical sources. Robotica, 22(01):109-115, 2004.
}
\author{Juliane Brink}
\examples{
pth = system.file("data",package="mobisens")
fnb="cs"
fne=".csv"
sensorStartAt(15000,10000,60,pth,fnb,fne,500,500,100,100,246,246,0)
forward(500,500)
bacteria(500,stop=50)
plotmobisensBw()

sensorStartAt(15000,10000,60,pth,fnb,fne,500,500,100,100,246,246,0)
forward(500,500)
planarian(60,400,stop=50)
plotmobisensBw()

sensorStartAt(14000,9000,60,pth,fnb,fne,500,500,100,100,246,246,0)
spiral_ferri(500,500,1,0.5,4,1,stop=50)
plotmobisensBw()
PI(1,0.5,4,1)

}

