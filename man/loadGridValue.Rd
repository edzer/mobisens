\name{loadGridValue}
\alias{loadGridValue}
\title{Load one value from a grid in a file}
\description{Returns the grid value at a given point from a grid in a file.}
\usage{loadGridValue(t, point, path, fnb, fne, xmin, ymin, deltax, deltay, nx, ny)}
\arguments{
  \item{t}{time step to be loaded}
  \item{point}{Point in the grid}
  \item{path}{directory containing the grid data}
  \item{fnb}{string the filenames begin with, followed by timestep}
  \item{fne}{string the filenames end with (after timestep)}
  \item{xmin}{x coordinate the grid begins with}
  \item{ymin}{y coordinate the grid begins with}
  \item{deltax}{distance represented by one pixel in x direction}
  \item{deltay}{distance represented by one pixel in y direction}
  \item{nx}{number of pixels in x direction}
  \item{ny}{number of pixels in x direction}
}
\value{value of the grid at the given point}
\author{Juliane Brink, Ragnar Warrlich}
\examples{
p = SpatialPoints(cbind(10000,10000))
pth = system.file("data",package="mobisens")
fnb="mp"
fne=".csv"
loadGridValue(50,p,pth,fnb,fne,500,500,100,100,246,246)
}
