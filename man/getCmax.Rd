\name{getCmax}
\alias{getCmax}
\title{Get the maximum concentration measured in the last plume crossing}
\description{
Returns the maximum concentration measured by the sensor in the last plume crossing.
}
\usage{
getCmax(thres, mobisensEnv)
}
\arguments{
  \item{thres}{measurement threshold, above threshold means inside the plume}
  \item{mobisensEnv}{optional, use different environments for multiple mobile sensors}
}
\value{
maximum measured concentration
}
\references{
Brink, J. and Pebesma, E. (2013), Plume Tracking with a Mobile Sensor Based on Incomplete and Imprecise Information. Transactions in GIS. doi: 10.1111/tgis.12063
}
\author{Juliane Brink}
\examples{
pth = system.file("data",package="mobisens")
fnb="mp"
fne=".csv"
sensorStart(pth,fnb,fne,500,500,100,100,246,246,0)
turn(45)
forward(10000,400)
getCmax(0.0001)
}
