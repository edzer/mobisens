\name{directions}
\docType{data}
\alias{directions}
\alias{quantdirections}
\alias{vectors}
\alias{angles}
\alias{orientation}
\alias{toside}
\alias{quantdirections}
\alias{quantdirections}

\title{Data sets for handling directions.}
\description{Lists containing cardinal directions, matrices containing orientation angles, etc.}
\usage{directions}
\format{lists and matrices}
\keyword{datasets}