\name{plotGrids}
\alias{plotGrids}
\alias{plotGridT}
\title{Plot one or multiple time steps of a data grid}
\description{Functions to plot one or multiple time steps of a data grid. Values of the grid(s) are log-transformed.}
\usage{
plotGrids(path, fnb, fne, threshold, t1, t2, int, xmin, ymin, deltax, deltay, nx, ny)
plotGridT(path, fnb, fne, threshold, t, xmin, ymin, deltax, deltay, nx, ny)
}
\arguments{
  \item{path}{directory containing the grid data}
  \item{fnb}{string the filenames begin with, followed by timestep}
  \item{fne}{string the filenames end with (after timestep)}
  \item{threshold}{numerical threshold, field values below threshold will be plotted as if value=0}
  \item{t}{time step to be plottet with plotGridT()}
  \item{t1}{first of multiple time steps to be plotted with plotGrids()}
  \item{t2}{final time step to be plottet with plotGrids(), only included in the plot if t2-t1 is a multiple of int}
  \item{int}{time interval between two plottet grids, for example 2 to plot every second time step}
  \item{xmin}{x coordinate the grid begins with}
  \item{ymin}{y coordinate the grid begins with}
  \item{deltax}{distance represented by one pixel in x direction}
  \item{deltay}{distance represented by one pixel in y direction}
  \item{nx}{number of pixels in x direction}
  \item{ny}{number of pixels in y direction}
}
\author{Juliane Brink, Ragnar Warrlich}
\seealso{plotmobisens(), plotmobisensBw()}
\examples{
pth = system.file("data",package="mobisens")
fnb="mp"
fne=".csv"
plotGrids(pth,fnb,fne,0.0001,2,12,2,500,500,100,100,246,246)
plotGridT(pth,fnb,fne,0.0001,20,500,500,100,100,246,246)
}



